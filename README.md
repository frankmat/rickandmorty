# Rick and Morty Android application

The application was implemented according to this design, but I was not the one who made the design. [figma rick and morty design](https://www.figma.com/file/0w76BZ8TviO2TQF84574Et/Rick-and-Morty-Test-Task-Design?node-id=0%3A1521) Application is using data from [Rick and Morty API](https://rickandmortyapi.com/)

## Key words
 - Kotlin
 - Jetpack Compose
 - Material Design
 - MVVM
 - Dependency injection
 - Rest API
 - Caching
 - Paging
 - Full Text Search