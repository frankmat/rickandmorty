package cz.cvut.fit.biand.rickandmorty.view


import androidx.compose.animation.AnimatedContent
import androidx.compose.animation.Crossfade
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.gestures.animateScrollBy
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.platform.LocalLifecycleOwner
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.flowWithLifecycle
import androidx.paging.PagingData
import androidx.paging.compose.collectAsLazyPagingItems
import cz.cvut.fit.biand.rickandmorty.R
import cz.cvut.fit.biand.rickandmorty.helpers.CharactersScreenState
import cz.cvut.fit.biand.rickandmorty.helpers.Tabs
import cz.cvut.fit.biand.rickandmorty.data.ui.Character
import cz.cvut.fit.biand.rickandmorty.viewmodel.SearchViewModel
import com.google.accompanist.pager.ExperimentalPagerApi
import com.google.accompanist.pager.HorizontalPager
import com.google.accompanist.pager.rememberPagerState
import com.ramcosta.composedestinations.annotation.Destination
import cz.cvut.fit.biand.rickandmorty.view.destinations.DetailScreenDestination
import com.ramcosta.composedestinations.navigation.DestinationsNavigator
import com.ramcosta.composedestinations.navigation.EmptyDestinationsNavigator
import cz.cvut.fit.biand.rickandmorty.ui.theme.RickAndMortyTheme
import kotlinx.coroutines.flow.Flow
import androidx.paging.LoadState
import androidx.paging.compose.LazyPagingItems
import androidx.paging.compose.items
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext

@Destination(start = true)
@OptIn(ExperimentalPagerApi::class)
@Composable
fun MainScreen(
    viewModel: SearchViewModel = hiltViewModel(),
    destinationsNavigator: DestinationsNavigator
) {
    val tabs = listOf(Tabs.ALL, Tabs.FAVORITES)
    val tabPages: List<@Composable () -> Unit> = listOf({
        ExploreScreen(
            viewModel.allCharactersFlow,
            viewModel.searchCharactersFlow,
            viewModel.searchQuery.value,
            viewModel.topBarState.value,
            viewModel::onInputBarChange,
            viewModel::onInputTrailingIconClick,
            viewModel::onSearchIconClick,
            viewModel::onBackButtonClick,
            destinationsNavigator,
        )
    }, {
        FavoritesScreen(
            viewModel.allCharactersFavoriteFlow,
            destinationsNavigator
        )
    })
    val pagerState = rememberPagerState(tabs.size)
    Scaffold(bottomBar = { TabRow(tabs = tabs, pagerState = pagerState) }) { padding ->
        HorizontalPager(
            state = pagerState, modifier = Modifier
                .padding(padding)
                .fillMaxSize()
        ) { page ->
            tabPages[page]()
        }
    }
}


@Composable
fun startCollectingFlow(charactersAllFlow: Flow<PagingData<Character>>): Flow<PagingData<Character>> {
    val lifecycleOwner = LocalLifecycleOwner.current
    val charactersFlowLifecycleAware = remember(charactersAllFlow, lifecycleOwner) {
        charactersAllFlow.flowWithLifecycle(lifecycleOwner.lifecycle, Lifecycle.State.STARTED)
    }
    return charactersFlowLifecycleAware
}

@Composable
fun FavoritesScreen(
    charactersFlow: Flow<PagingData<Character>>,
    destinationNavigator: DestinationsNavigator
) {
    val characters = startCollectingFlow(charactersFlow).collectAsLazyPagingItems()
    Scaffold(topBar = { TopBarFavorites() }) {

        RefreshStatusManager(characters = characters, onCardClick = {
            destinationNavigator.navigate(
                DetailScreenDestination(
                    it
                )
            )
        }, topBarState = CharactersScreenState.ALL, emptyContent = {
            EmptyScreenFavorites()

        })
    }
}


@OptIn(ExperimentalAnimationApi::class)
@Composable
fun ExploreScreen(
    charactersAllFlow: Flow<PagingData<Character>>,
    charactersSearchFlow: Flow<PagingData<Character>>,
    searchQuery: String,
    topBarState: CharactersScreenState,
    onInputBarChange: (String) -> Unit,
    onInputTrailingIconClick: () -> Unit,
    onSearchIconClick: () -> Unit,
    onBackButtonClick: () -> Unit,
    destinationsNavigator: DestinationsNavigator
) {
    val lazyListStateAll:LazyListState = rememberLazyListState()
    val lazyListStateSearch:LazyListState = rememberLazyListState()

    AnimatedContent(targetState = topBarState) {
        when (it) {
            CharactersScreenState.ALL -> AllCharactersScreen(
                charactersAllFlow,
                topBarState,
                destinationsNavigator,
                onSearchIconClick,
                lazyListStateAll
            )
            CharactersScreenState.SEARCH -> SearchScreen(
                charactersSearchFlow,
                topBarState,
                destinationsNavigator,
                onBackButtonClick,
                onInputBarChange,
                onInputTrailingIconClick,
                searchQuery,
                lazyListStateSearch
            )
        }
    }
}


@Composable
fun AllCharactersScreen(
    charactersAllFlow: Flow<PagingData<Character>>,
    topBarState: CharactersScreenState,
    destinationsNavigator: DestinationsNavigator,
    onSearchIconClick: () -> Unit,
    lazyListState:LazyListState = rememberLazyListState()
) {
    val characters = startCollectingFlow(charactersAllFlow).collectAsLazyPagingItems()
    Scaffold(Modifier.fillMaxSize(), topBar = {
        AllCharactersTopBar(
            onSearchIconClick
        )
    }) {
        characters.loadState.mediator.let { mediator ->
            when (mediator) {
                null -> CenteredCircularProgressBar()
                else ->
                    RefreshStatusManager(
                        characters,
                        { character: Character ->
                            destinationsNavigator.navigate(
                                DetailScreenDestination(character)
                            )
                        },
                        topBarState,
                        emptyContent = { onRetry ->
                            EmptyScreenAll(
                                onRetry
                            )
                        },
                        lazyListState
                    )
            }
        }
    }
}


@Composable
fun SearchScreen(
    charactersSearchFlow: Flow<PagingData<Character>>,
    topBarState: CharactersScreenState,
    destinationsNavigator: DestinationsNavigator,
    onBackButtonClick: () -> Unit,
    onInputBarChange: (String) -> Unit,
    onInputTrailingIconClick: () -> Unit,
    searchQuery: String,
    lazyListState:LazyListState = rememberLazyListState()
) {
    val characters = startCollectingFlow(charactersSearchFlow).collectAsLazyPagingItems()
    val coroutineScope= rememberCoroutineScope()
    Scaffold(Modifier.fillMaxSize(), topBar = {
        SearchTopBar(
            { newValue ->
                onInputBarChange(newValue)
                coroutineScope.launch { lazyListState.animateScrollToItem(0) }
                characters.refresh() },
            {
                onInputTrailingIconClick()
                coroutineScope.launch { lazyListState.animateScrollToItem(0) }
                characters.refresh()
            },
            onBackButtonClick,
            searchQuery
        )
    }) {
        RefreshStatusManager(
            characters,
            { character: Character ->
                destinationsNavigator.navigate(
                    DetailScreenDestination(character)
                )
            },
            topBarState,
            emptyContent = { },
            lazyListState
        )
    }
}


@Composable
fun RefreshStatusManager(
    characters: LazyPagingItems<Character>,
    onCardClick: (Character) -> Unit,
    topBarState: CharactersScreenState,
    emptyContent: @Composable (() -> Unit) -> Unit,
    lazyListState:LazyListState = rememberLazyListState()
) {
    Crossfade(characters.loadState.refresh) { refreshState ->
        when (refreshState) {
            is LoadState.Loading -> CenteredCircularProgressBar()
            is LoadState.Error -> emptyContent { characters.retry() }
            is LoadState.NotLoading ->
                ItemsManager(
                    characters,
                    topBarState,
                    onCardClick,
                    emptyContent,
                    lazyListState
                )
        }
    }
}


@Composable
fun ItemsManager(
    characters: LazyPagingItems<Character>,
    topBarState: CharactersScreenState,
    onCardClick: (Character) -> Unit,
    emptyContent: @Composable (() -> Unit) -> Unit,
    lazyListState:LazyListState = rememberLazyListState()
) {
    characters.itemCount.let { itemCount ->
        when {
            itemCount != 0 ->
                LazyColumnOfCharacters(
                    characters,
                    topBarState,
                    onCardClick,
                    lazyListState
                )
            itemCount == 0 ->
                Crossfade(targetState = characters.loadState.mediator) { mediator ->
                    when (mediator) {
                        null -> emptyContent {}
                        else -> CenteredCircularProgressBar()
                    }
                }
        }
    }
}


@Composable
fun LazyColumnOfCharacters(
    characters: LazyPagingItems<Character>,
    topBarState: CharactersScreenState,
    onCardClick: (Character) -> Unit,
    lazyListState:LazyListState = rememberLazyListState()
) {
    LazyColumn(
        state=lazyListState,
        horizontalAlignment = Alignment.CenterHorizontally,
    ) {
        items(characters) { character ->
            character?.let {
                SearchPreviewCard(
                    character = it,
                    topBarState,
                    onCardClick = {
                        onCardClick(it)
                    },
                    image = { photo ->
                        OwnGlideImage(
                            photo = photo,
                            Modifier
                                .padding(8.dp)
                                .size(52.dp)
                                .clip(RoundedCornerShape(8.dp))
                        )
                    }
                )
            }
        }
        item {
            Crossfade(targetState = characters.loadState.append) { appendLoadState ->
                when (appendLoadState) {
                    is LoadState.Error -> CenteredRetryButton { characters.retry()}
                    is LoadState.Loading -> CenteredCircularProgressBar()
                    is LoadState.NotLoading -> {}
                }
            }
        }
    }
}


@Composable
fun EmptyScreenAll(onRetry: () -> Unit) {
    EmptyScreen(
        text = stringResource(id = R.string.characters_empty),
        shouldEnableRefresh = true,
        onRefresh = onRetry
    )

}

@Composable
fun EmptyScreenFavorites() {
    EmptyScreen(text = stringResource(id = R.string.favorites_empty))
}

@Composable
fun EmptyScreen(
    text: String = "",
    shouldEnableRefresh: Boolean = false,
    onRefresh: () -> Unit = {}
) {
    Column(
        modifier = Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text(
            modifier = Modifier.padding(bottom = 16.dp),
            text = text,
            color = MaterialTheme.colors.onSecondary,
            style = MaterialTheme.typography.body2
        )
        if (shouldEnableRefresh) {
            RetryButton(onRefresh)
        }
    }
}

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun SearchPreviewCard(
    character: Character,
    screenState: CharactersScreenState,
    onCardClick: () -> Unit,
    image: @Composable (String) -> Unit

) {
    val padding = PaddingValues(top = 4.dp, start = 8.dp, end = 8.dp, bottom = 4.dp)
    Card(
        modifier = Modifier
            .padding(padding)
            .fillMaxWidth(),
        shape = RoundedCornerShape(16.dp),
        onClick = onCardClick,
        elevation = screenState.elevation,
        backgroundColor = MaterialTheme.colors.surface.copy(screenState.transparentAlpha)

    ) {
        SearchPreviewCardContent(character = character, image)
    }
}

@Composable
fun SearchPreviewCardContent(character: Character, image: @Composable (String) -> Unit) {
    with(character) {
        Row(verticalAlignment = Alignment.Top) {
            image(photo)
            Column(modifier = Modifier.padding(8.dp)) {
                Row(verticalAlignment = Alignment.CenterVertically) {
                    Text(
                        text = name,
                        modifier = Modifier.padding(end = 5.dp),
                        style = MaterialTheme.typography.h3
                    )
                    if (isFavorite) {
                        FilledStar()
                    }
                }
                Text(
                    status,
                    style = MaterialTheme.typography.body1,
                    color = MaterialTheme.colors.secondaryVariant
                )
            }
        }
    }
}


@Preview(showBackground = true)
@Composable
fun PreviewCardAllPreview() {
    PreviewCardContentMock(screenState = CharactersScreenState.ALL)
}

@Preview(showBackground = true)
@Composable
fun PreviewCardSearchPreview() {
    PreviewCardContentMock(screenState = CharactersScreenState.SEARCH)
}

@Composable
fun PreviewCardContentMock(screenState: CharactersScreenState) {
    RickAndMortyTheme {
        val rick = Character(
            1,
            "Rick Sanchez",
            "Alive",
            "Human",
            "-",
            "Male",
            "Earth (C-137)",
            "Earth (Replacement Dimension)",
            "",
            true
        )
        SearchPreviewCard(
            character = rick,
            screenState,
            {},
            {
                Image(
                    painter = painterResource(id = R.drawable.abadango),
                    contentDescription = stringResource(
                        id = R.string.photo_preview,
                        rick.name
                    ),
                    modifier = Modifier
                        .padding(8.dp)
                        .size(52.dp)
                        .clip(RoundedCornerShape(8.dp))
                )
            })
    }
}


@Preview(showBackground = true)
@Composable
fun SearchScreenPreview() {
    RickAndMortyTheme {
        ExploreScreen(
            charactersAllFlow = flow {},
            charactersSearchFlow = flow {},
            searchQuery = "",
            topBarState = CharactersScreenState.SEARCH,
            onInputBarChange = {},
            onInputTrailingIconClick = { },
            onSearchIconClick = { },
            onBackButtonClick = { },
            destinationsNavigator = EmptyDestinationsNavigator
        )
    }

}

