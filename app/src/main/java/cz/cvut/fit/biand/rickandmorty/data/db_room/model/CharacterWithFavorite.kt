package cz.cvut.fit.biand.rickandmorty.data.db_room.model

import androidx.paging.PagingData
import androidx.paging.map
import androidx.room.ColumnInfo
import cz.cvut.fit.biand.rickandmorty.data.ui.Character

data class CharacterWithFavorite(
    val id: Int?,
    val name: String?,
    val status: String?,
    val species: String?,
    val type: String?,
    val gender: String?,
    val origin: String?,
    val location: String?,
    val photo: String?,
    @ColumnInfo(name = "idFromFavorites") val idFromFavorites: Int? = null
){
    fun toCharacterUI(): Character =
        Character(
            id ?: 0,
            name ?: "",
            status ?: "",
            species ?: "",
            type ?: "-",
            gender ?: "",
            origin ?: "",
            location ?: "",
            photo ?: "",
            idFromFavorites!=null
        )
}
fun PagingData<CharacterWithFavorite>.toPagingDataOfUICharacters(): PagingData<Character> {
    return map { it.toCharacterUI() }
}