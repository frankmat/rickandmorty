package cz.cvut.fit.biand.rickandmorty.data.retrofit.model

import com.google.gson.annotations.SerializedName
import cz.cvut.fit.biand.rickandmorty.data.db_room.model.DbCharacter

data class RetrofitCharacter(
    @SerializedName("id")
    val id: Int,
    @SerializedName("name")
    val name: String,
    @SerializedName("status")
    val status: String,
    @SerializedName("species")
    val species: String,
    @SerializedName("type")
    val type: String,
    @SerializedName("gender")
    val gender: String,
    @SerializedName("origin")
    val origin: RetrofitPlanet,
    @SerializedName("location")
    val location: RetrofitPlanet,
    @SerializedName("image")
    val image: String,
    @SerializedName("episode")
    val episode: List<String>,
    @SerializedName("url")
    val url: String,
    @SerializedName("created")
    val created: String
){
    fun toDbCharacter(): DbCharacter = DbCharacter(
        id, name, status, species, type.ifEmpty { "-" }, gender, origin.name, location.name, image
    )
}

fun List<RetrofitCharacter>.toDbCharacters():List<DbCharacter> = this.map { it.toDbCharacter() }

