package cz.cvut.fit.biand.rickandmorty.data.retrofit

import cz.cvut.fit.biand.rickandmorty.helpers.Constants
import cz.cvut.fit.biand.rickandmorty.data.retrofit.model.RetrofitCharacterPage
import retrofit2.http.GET
import retrofit2.http.Query


interface RickAndMortyRemoteService {

    @GET(Constants.CHARACTERS_ENDPOINT)
    suspend fun getCharacters(
        @Query("page") page:Int
    ): RetrofitCharacterPage
}