package cz.cvut.fit.biand.rickandmorty.di
import cz.cvut.fit.biand.rickandmorty.RickAndMortyRoomDatabase
import cz.cvut.fit.biand.rickandmorty.data.db_room.CharactersDao
import cz.cvut.fit.biand.rickandmorty.data.retrofit.RickAndMortyRemoteService
import cz.cvut.fit.biand.rickandmorty.repository.CharactersMediator
import cz.cvut.fit.biand.rickandmorty.repository.CharactersRepository
import cz.cvut.fit.biand.rickandmorty.repository.CharactersRepositoryImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton


@Module
@InstallIn(SingletonComponent::class)
object AppModule {


    @Singleton
    @Provides
    fun providesCharactersMediator(
        charactersDao: CharactersDao,
        charactersRemoteService: RickAndMortyRemoteService,
        db: RickAndMortyRoomDatabase
    ) = CharactersMediator(db, charactersDao, charactersRemoteService)

    @Singleton
    @Provides
    fun providesCharacterRepoImpl(
        dao: CharactersDao,
        charactersMediator: CharactersMediator,
    ): CharactersRepository =
        CharactersRepositoryImpl(dao, charactersMediator)




}