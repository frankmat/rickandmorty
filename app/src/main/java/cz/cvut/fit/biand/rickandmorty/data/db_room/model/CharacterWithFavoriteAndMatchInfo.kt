package cz.cvut.fit.biand.rickandmorty.data.db_room.model

import androidx.paging.PagingData
import androidx.paging.map
import androidx.room.ColumnInfo
import androidx.room.Embedded
import cz.cvut.fit.biand.rickandmorty.data.ui.Character

data class CharacterWithFavoriteAndMatchInfo(
    @Embedded
    val character: DbCharacter,
    @ColumnInfo(name = "matchInfo")
    val matchInfo: ByteArray,
    @ColumnInfo(name = "idFromFavorites") val idFromFavorites: Int? = null


) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as CharacterWithFavoriteAndMatchInfo

        if (character != other.character) return false
        if (!matchInfo.contentEquals(other.matchInfo)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = character.hashCode()
        result = 31 * result + matchInfo.contentHashCode()
        return result
    }

    fun toUICharacter(): Character {
        with(character) {
            return Character(
                id ?: 0,
                name ?: "",
                status ?: "",
                species ?: "",
                type ?: "-",
                gender ?: "",
                origin ?: "",
                location ?: "",
                photo ?: "",
                idFromFavorites != null
            )
        }
    }
}

fun PagingData<CharacterWithFavoriteAndMatchInfo>.extractPagingDataOfUICharacters(): PagingData<Character> {
    return map { it.toUICharacter() }
}