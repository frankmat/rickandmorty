package cz.cvut.fit.biand.rickandmorty.data.db_room

import androidx.paging.PagingSource
import androidx.room.*
import cz.cvut.fit.biand.rickandmorty.data.db_room.model.*

@Dao
interface CharactersDao {

   

    @Query("SELECT count(id) FROM characters")
    suspend fun getNumberOfCharacters():Int

    @Delete
    suspend  fun removeFromFavorites(favorite: Favorite)

    @Insert
    suspend fun addToFavorites(favorite: Favorite)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertCharacters(dbCharacters: List<DbCharacter>)

    @Query("DELETE FROM characters")
    suspend fun deleteCharacters()



    @Query("SELECT characters.id as id,name,status,species,type,gender,origin,location,photo,favorites.characterID as idFromFavorites " +
            "FROM characters " +
            "LEFT JOIN favorites ON favorites.userName=:userName AND characters.id = favorites.characterID"
    )
    fun getAllCharactersPagingSource(userName: String): PagingSource<Int, CharacterWithFavorite>


    @Query("SELECT characters.id as id,name,status,species,type,gender,origin,location,photo,favorites.characterID as idFromFavorites " +
            "FROM characters " +
            "INNER JOIN favorites ON favorites.userName=:userName AND characters.id = favorites.characterID")
    fun getFavoriteCharactersPagingSource(userName: String): PagingSource<Int, CharacterWithFavorite>

    
    @Query("SELECT " +
            "distinct characters.id as id,characters.name,status,species,type," +
            "gender,origin,location,photo,favorites.characterID as idFromFavorites,matchInfo " +
            "FROM (SELECT characters.id,characters.name,status,species,type,gender,origin,location,photo, matchinfo(characters_fts) as matchInfo FROM characters JOIN characters_fts ON characters.name=characters_fts.name WHERE characters_fts.name MATCH :query ) as characters " +
            "LEFT JOIN favorites ON favorites.userName=:userName AND id = favorites.characterID " +
            "ORDER BY matchInfo"
    )
    fun getSearchedCharactersByNamePagingSource(userName: String, query: String): PagingSource<Int, CharacterWithFavoriteAndMatchInfo>

}


