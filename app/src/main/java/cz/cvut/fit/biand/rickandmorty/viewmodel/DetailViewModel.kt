package cz.cvut.fit.biand.rickandmorty.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import cz.cvut.fit.biand.rickandmorty.repository.CharactersRepository
import cz.cvut.fit.biand.rickandmorty.data.db_room.model.Favorite
import cz.cvut.fit.biand.rickandmorty.data.db_room.model.User
import cz.cvut.fit.biand.rickandmorty.helpers.Constants
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class DetailViewModel @Inject constructor (private val charactersRepository: CharactersRepository) : ViewModel() {

    private val user = User(Constants.USERNAME)

    fun onStarClick(characterID: Int, isFavorite: Boolean) =
        viewModelScope.launch(Dispatchers.IO) {
            if (isFavorite) charactersRepository.removeFromFavorites(Favorite(characterID,user.userName))
            else charactersRepository.addToFavorites(Favorite(characterID,user.userName))

        }
}
