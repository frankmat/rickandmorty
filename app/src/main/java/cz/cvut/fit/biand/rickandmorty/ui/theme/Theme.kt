package cz.cvut.fit.biand.rickandmorty.ui.theme

import android.annotation.SuppressLint

import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material.MaterialTheme
import androidx.compose.material.darkColors
import androidx.compose.material.lightColors

import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color

@SuppressLint("ConflictingOnColor")
private val DarkColorPalette = darkColors(
    primary = DarkDarkBlack,
    secondary = DarkPurple,
    secondaryVariant = DarkGray,
    background = DarkDarkBlack,
    surface = DarkLightBlack,

    onPrimary = Color.White,
    onSecondary = DarkLightGray,
    onSurface = Color.White
)

@SuppressLint("ConflictingOnColor")
private val LightColorPalette = lightColors(
    primary = LightWhiteLittleBitPurple,
    secondary = LightPurple,
    secondaryVariant = LightGray,
    background = LightWhiteLittleBitPurple,
    surface = Color.White,

    onPrimary = Color.Black,
    onSecondary = LightLightGray,
    onSurface = Color.Black
)

@Composable
fun RickAndMortyTheme(darkTheme: Boolean = isSystemInDarkTheme(), content: @Composable () -> Unit) {
    val colors = if (darkTheme) {
        DarkColorPalette
    } else {
        LightColorPalette
    }

    MaterialTheme(
        colors = colors,
        typography = Typography,
        shapes = Shapes,
        content = content
    )
}