package cz.cvut.fit.biand.rickandmorty.data.retrofit.model

import com.google.gson.annotations.SerializedName

data class RetrofitCharacterPage(
    @SerializedName("info")
    val info: RetrofitPageInfo,
    @SerializedName("results")
    val results: List<RetrofitCharacter>
)
