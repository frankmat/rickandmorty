package cz.cvut.fit.biand.rickandmorty.repository

import androidx.paging.ExperimentalPagingApi
import androidx.paging.PagingSource
import cz.cvut.fit.biand.rickandmorty.data.db_room.model.*


interface CharactersRepository {
     suspend fun removeFromFavorites(favorite: Favorite)
     suspend fun addToFavorites(favorite: Favorite)
     @OptIn(ExperimentalPagingApi::class)
     fun getAllCharactersMediator():CharactersMediator
     fun getAllCharactersPagingSource(user: User):PagingSource<Int,CharacterWithFavorite>
     fun getFavoriteCharactersPagingSource(user: User):PagingSource<Int,CharacterWithFavorite>
     fun getSearchedCharactersByNamePagingSource(user: User,query: String):PagingSource<Int,CharacterWithFavoriteAndMatchInfo>



}