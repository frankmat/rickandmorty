package cz.cvut.fit.biand.rickandmorty.helpers

object Constants {
    const val RICK_AND_MORTY_URL="https://rickandmortyapi.com/api/"
    const val CHARACTERS_ENDPOINT = "character"
    const val DATABASE_NAME="characters_db"
    const val PAGE_SIZE = 20
    const val USERNAME = "user2"
}