package cz.cvut.fit.biand.rickandmorty.view

import androidx.compose.animation.Crossfade
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.Search
import androidx.compose.material.icons.rounded.Star
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.skydoves.landscapist.glide.GlideImage
import cz.cvut.fit.biand.rickandmorty.R
import cz.cvut.fit.biand.rickandmorty.ui.theme.RickAndMortyTheme


@Composable
fun AnimatedStar(isFavorite: Boolean = false,onIconClick:(Boolean)->Unit) {
    val isFavoriteState= rememberSaveable {
        mutableStateOf(isFavorite)
    }
    IconButton(onClick = {  onIconClick(isFavoriteState.value) ;isFavoriteState.value = !isFavoriteState.value}) {
        Crossfade(targetState = isFavoriteState) {
            when (it.value) {
                true -> FilledStar()
                false -> OutlinedStar()
            }
        }
    }

}


@Composable
fun FilledStar(tint: Color = MaterialTheme.colors.secondary) {
    Icon(
        imageVector = Icons.Rounded.Star,
        contentDescription = stringResource(id = R.string.icon_favorite),
        tint = tint, modifier = Modifier.size(30.dp)
    )
}

@Composable
fun OutlinedStar() {
    Icon(
        painter = painterResource(id = R.drawable.round_star_border_24),
        contentDescription = stringResource(id = R.string.icon_not_favorite),
        tint = MaterialTheme.colors.onPrimary,
        modifier = Modifier.size(30.dp)
    )
}

@Composable
fun SearchIcon() {
    Icon(
        imageVector = Icons.Outlined.Search,
        contentDescription = stringResource(id = R.string.icon_search),
        modifier=Modifier.size(24.dp)
    )
}

@Composable
fun CenteredCircularProgressBar() {
    Row(
        modifier = Modifier.fillMaxSize(),
        horizontalArrangement = Arrangement.Center,
        verticalAlignment = Alignment.CenterVertically
    ) { CircularProgressIndicator(color = MaterialTheme.colors.secondary) }
}



@Composable
fun OwnGlideImage(photo: String, modifier: Modifier) {
    GlideImage(
        imageModel = photo,
        modifier = modifier,
        contentDescription = stringResource(id = R.string.photo_preview, photo)
    )
}


@Composable
fun RetryButton(onRefresh: () -> Unit) {
    Button(onClick = onRefresh) {
        Text(text = stringResource(id = R.string.click_to_refresh))
    }
}

@Composable
fun CenteredRetryButton(onRefresh: () -> Unit) {
    Row(
        modifier = Modifier.fillMaxWidth(),
        horizontalArrangement = Arrangement.Center,
        verticalAlignment = Alignment.CenterVertically
    ) {
        RetryButton(onRefresh)
    }
}


@Preview(showBackground = true)
@Composable
fun FavoriteStarPreview() {
    RickAndMortyTheme {
        AnimatedStar(false) {}
    }
}