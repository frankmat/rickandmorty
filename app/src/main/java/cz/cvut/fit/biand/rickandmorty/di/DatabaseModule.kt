package cz.cvut.fit.biand.rickandmorty.di

import android.content.Context
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import cz.cvut.fit.biand.rickandmorty.RickAndMortyRoomDatabase
import cz.cvut.fit.biand.rickandmorty.data.db_room.CharactersDao
import cz.cvut.fit.biand.rickandmorty.helpers.Constants
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton


@Module
@InstallIn(SingletonComponent::class)
object DatabaseModule {
    @Singleton
    @Provides
    fun providesDatabase(@ApplicationContext app: Context): RickAndMortyRoomDatabase = Room.databaseBuilder(
        app,
        RickAndMortyRoomDatabase::class.java,
        Constants.DATABASE_NAME
    )
        .addCallback(object : RoomDatabase.Callback() {
            override fun onCreate(db: SupportSQLiteDatabase) {
                super.onCreate(db)
                db.execSQL("INSERT INTO characters_fts(characters_fts) VALUES ('rebuild')")
            }
        })
        .fallbackToDestructiveMigration()
        .build()

    @Singleton
    @Provides
    fun providesCharactersDao(db: RickAndMortyRoomDatabase): CharactersDao = db.charactersDao()
}