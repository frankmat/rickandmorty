package cz.cvut.fit.biand.rickandmorty.helpers

import androidx.annotation.StringRes
import cz.cvut.fit.biand.rickandmorty.R

enum class Tabs(@StringRes val title:Int, val icon: Int) {
    ALL(R.string.characters, R.drawable.ic_group_19),
    FAVORITES(R.string.favorites, R.drawable.round_star_20)
}