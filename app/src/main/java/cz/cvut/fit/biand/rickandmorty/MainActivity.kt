package cz.cvut.fit.biand.rickandmorty

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material.MaterialTheme
import androidx.core.view.WindowInsetsControllerCompat
import com.ramcosta.composedestinations.DestinationsNavHost
import cz.cvut.fit.biand.rickandmorty.ui.theme.RickAndMortyTheme
import cz.cvut.fit.biand.rickandmorty.view.NavGraphs
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            RickAndMortyTheme {
                window.statusBarColor = MaterialTheme.colors.primary.hashCode()
                WindowInsetsControllerCompat(window, window.decorView).isAppearanceLightStatusBars =
                    !isSystemInDarkTheme()

                DestinationsNavHost(navGraph = NavGraphs.root)
            }
        }
    }
}