package cz.cvut.fit.biand.rickandmorty.data.ui


import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Character(
    val id: Int,
    val name: String,
    val status: String,
    val species: String,
    val type: String,
    val gender: String,
    val origin: String,
    val location: String,
    val photo: String,
    val isFavorite: Boolean
): Parcelable
