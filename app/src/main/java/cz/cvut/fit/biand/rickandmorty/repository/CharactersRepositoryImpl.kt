package cz.cvut.fit.biand.rickandmorty.repository


import androidx.paging.ExperimentalPagingApi
import androidx.paging.PagingSource
import cz.cvut.fit.biand.rickandmorty.data.db_room.CharactersDao
import cz.cvut.fit.biand.rickandmorty.data.db_room.model.*


class CharactersRepositoryImpl(
    private val charactersDao: CharactersDao,
    private val charactersMediator: CharactersMediator
) : CharactersRepository {


    override suspend fun removeFromFavorites(favorite: Favorite): Unit =
        charactersDao.removeFromFavorites(favorite)

    override suspend fun addToFavorites(favorite: Favorite): Unit =
        charactersDao.addToFavorites(favorite)

    @OptIn(ExperimentalPagingApi::class)
    override fun getAllCharactersMediator(): CharactersMediator = charactersMediator

    override fun getAllCharactersPagingSource(user: User): PagingSource<Int,CharacterWithFavorite> = charactersDao.getAllCharactersPagingSource(user.userName)

    override fun getFavoriteCharactersPagingSource(user: User): PagingSource<Int, CharacterWithFavorite> = charactersDao.getFavoriteCharactersPagingSource(user.userName)

    override fun getSearchedCharactersByNamePagingSource(user: User,query: String): PagingSource<Int, CharacterWithFavoriteAndMatchInfo> = charactersDao.getSearchedCharactersByNamePagingSource(user.userName,query)
}
