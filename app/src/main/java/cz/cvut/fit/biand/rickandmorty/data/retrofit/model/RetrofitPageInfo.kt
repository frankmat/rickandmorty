package cz.cvut.fit.biand.rickandmorty.data.retrofit.model

import com.google.gson.annotations.SerializedName

data class RetrofitPageInfo(
    @SerializedName("count")
    val count: Int,
    @SerializedName("pages")
    val pages: Int,
    @SerializedName("next")
    val next: String?,
    @SerializedName("prev")
    val prev: String?
    )
