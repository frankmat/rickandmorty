package cz.cvut.fit.biand.rickandmorty.ui.theme

import androidx.compose.ui.graphics.Color

val LightWhiteLittleBitPurple=Color(0xFFF4F4F9)
val LightPurple=Color(0xFF0000FF)
val LightGray = Color(0xFF666666)
val LightLightGray= Color(0xFFB3B3B3)


val DarkDarkBlack = Color (0xFF181819)
val DarkPurple = Color(0xFF9595FE)
val DarkGray = Color (0xFF8B8B8C)
val DarkLightBlack = Color(0xFF2E2E2F)
val DarkLightGray = Color(0xFF5D5D5E)
