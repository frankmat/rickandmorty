package cz.cvut.fit.biand.rickandmorty.helpers

import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp

enum class CharactersScreenState(val elevation: Dp, val transparentAlpha:Float) {
    ALL(3.dp,1f),SEARCH(0.dp,0f)
}