package cz.cvut.fit.biand.rickandmorty.di

import com.google.gson.GsonBuilder
import cz.cvut.fit.biand.rickandmorty.data.retrofit.RickAndMortyRemoteService
import cz.cvut.fit.biand.rickandmorty.helpers.Constants
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object RemoteApiModule {

    @Singleton
    @Provides
    fun provideHttpClient():OkHttpClient= OkHttpClient
        .Builder()
        .build()


    @Singleton
    @Provides
    fun providesRetrofit(okHttpClient: OkHttpClient): Retrofit =
        Retrofit.Builder()
            .baseUrl(Constants.RICK_AND_MORTY_URL)
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
            .build()




    @Singleton
    @Provides
    fun providesCharactersRemoteService(retrofit: Retrofit): RickAndMortyRemoteService =
        retrofit.create(RickAndMortyRemoteService::class.java)
}