package cz.cvut.fit.biand.rickandmorty.view

import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import cz.cvut.fit.biand.rickandmorty.helpers.Tabs
import com.google.accompanist.pager.ExperimentalPagerApi
import com.google.accompanist.pager.PagerState
import com.google.accompanist.pager.pagerTabIndicatorOffset
import com.google.accompanist.pager.rememberPagerState
import cz.cvut.fit.biand.rickandmorty.R
import kotlinx.coroutines.launch

@OptIn(ExperimentalPagerApi::class)
@Composable
fun TabRow(tabs: List<Tabs>, pagerState: PagerState) {
    Card(
        shape = RoundedCornerShape(topStart = 8.dp, topEnd = 8.dp),
        modifier = Modifier
            .shadow(15.dp),
        elevation = 10.dp
    ) {
        TabRow(
            selectedTabIndex = pagerState.currentPage,
            indicator = { tabPositions ->
                TabRowDefaults.Indicator(
                    Modifier.pagerTabIndicatorOffset(pagerState, tabPositions)
                )
            },
            backgroundColor = MaterialTheme.colors.surface,
        ) {
            tabs.forEachIndexed { index, tab ->
                PageItem(index, tab, pagerState)
            }
        }
    }
}

@OptIn(ExperimentalPagerApi::class)
@Composable
fun PageItem(index: Int, page: Tabs, pagerState: PagerState) {
    val scope = rememberCoroutineScope()
    Tab(
        text = { Text(stringResource(id = page.title)) },
        icon = {
            Icon(painterResource(id = page.icon), stringResource(id = R.string.bottom_tab,page.title))
        },
        selected = pagerState.currentPage == index,
        onClick = {
            scope.launch {
                pagerState.scrollToPage(index)
            }
        },
        selectedContentColor = MaterialTheme.colors.secondary,
        unselectedContentColor = MaterialTheme.colors.onSecondary
    )
}

@OptIn(ExperimentalPagerApi::class)
@Preview
@Composable
fun TabRowPreview() {
    val tabs=listOf(Tabs.ALL,Tabs.FAVORITES)
    val pagerState = rememberPagerState(tabs.size)
    TabRow(tabs = tabs , pagerState = pagerState)
}