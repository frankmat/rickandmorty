package cz.cvut.fit.biand.rickandmorty.data.db_room.model

import androidx.room.Entity

@Entity(tableName = "favorites",primaryKeys = ["characterID","userName"])
data class Favorite(
    val characterID:Int,
    val userName:String
)
