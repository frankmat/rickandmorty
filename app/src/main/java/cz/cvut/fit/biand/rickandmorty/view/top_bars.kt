package cz.cvut.fit.biand.rickandmorty.view

import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.KeyboardArrowLeft
import androidx.compose.material.icons.rounded.Close
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import cz.cvut.fit.biand.rickandmorty.R
import cz.cvut.fit.biand.rickandmorty.data.ui.Character
import cz.cvut.fit.biand.rickandmorty.ui.theme.RickAndMortyTheme

@Composable
fun TopBar(
    modifier: Modifier = Modifier,
    leadingIcon: ImageVector? = null,
    leadingIconAction: () -> Unit = {},
    text: String,
    placeHolderForTextField: String = "",
    showInputBar: Boolean = false,
    onInputBarChange: (String) -> Unit = {},
    onInputTrailingIconClick: () -> Unit = {},
    trailingIcon: @Composable () -> Unit = {}
) {
    Card(
        modifier = Modifier
            .fillMaxWidth()
            .shadow(15.dp), elevation = 10.dp,
        backgroundColor = MaterialTheme.colors.primary

    ) {
        Row(
            modifier = modifier
                .padding(top = 10.dp, start = 8.dp, bottom = 4.dp, end = 5.dp),
            horizontalArrangement = Arrangement.SpaceBetween,
            verticalAlignment = Alignment.CenterVertically,

            ) {
            Row(
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.Start

            ) {
                leadingIcon?.let {
                    IconButton(onClick = leadingIconAction) {
                        Icon(
                            imageVector = it,
                            contentDescription = stringResource(id = R.string.icon_back),
                            tint = MaterialTheme.colors.onPrimary
                        )
                    }
                }

                if (showInputBar) {
                    TextField(
                        value = text,
                        onValueChange = { onInputBarChange(it) },
                        colors = TextFieldDefaults.textFieldColors(
                            backgroundColor = MaterialTheme.colors.primary,
                            unfocusedIndicatorColor = MaterialTheme.colors.primary
                        ),
                        trailingIcon = {
                            IconButton(onClick = onInputTrailingIconClick) {
                                Icon(
                                    imageVector = Icons.Rounded.Close,
                                    contentDescription = stringResource(id = R.string.icon_cancel)
                                )
                            }
                        },
                        textStyle = MaterialTheme.typography.subtitle2,
                        modifier = Modifier.fillMaxWidth(),
                        placeholder = {
                            Text(
                                placeHolderForTextField,
                                style = MaterialTheme.typography.subtitle2,
                                color = MaterialTheme.colors.secondaryVariant
                            )
                        }
                    )
                } else {
                    Text(
                        text = text,
                        style = MaterialTheme.typography.h2,
                        color = MaterialTheme.colors.onPrimary,
                    )
                }

            }
            trailingIcon()
        }
    }
}


@Composable
fun AllCharactersTopBar(onSearchIconClick: () -> Unit) {
    TopBar(
        text = stringResource(id = R.string.characters),
        showInputBar = false,
        trailingIcon = {
            IconButton(
                onClick = onSearchIconClick
            ) {
                SearchIcon()
            }
        },
    )
}


@Composable
fun SearchTopBar(
    onInputBarChange: (String) -> Unit,
    onInputTrailingIconClick: () -> Unit,
    onBackButtonClick: () -> Unit,
    searchQuery: String
) {
    TopBar(
        text = searchQuery,
        leadingIcon = Icons.Filled.KeyboardArrowLeft,
        showInputBar = true,
        onInputBarChange = onInputBarChange,
        onInputTrailingIconClick = onInputTrailingIconClick,
        placeHolderForTextField = stringResource(id = R.string.search),
        leadingIconAction = onBackButtonClick
    )
}


@Composable
fun TopBarFavorites() {
    TopBar(
        text = stringResource(id = R.string.favorites),
        showInputBar = false,
        modifier = Modifier.padding(top = 10.dp, bottom = 7.dp)
    )
}


@Composable
fun TopBarDetail(
    character: Character,
    onBackButtonClick: () -> Unit,
    onStarClick: (Int, Boolean) -> Unit
) {

    TopBar(
        leadingIcon = Icons.Filled.KeyboardArrowLeft,
        leadingIconAction = onBackButtonClick,
        text = character.name
    ) { AnimatedStar(character.isFavorite) { onStarClick(character.id, it) } }
}


@Preview(showBackground = true)
@Composable
fun TopBarCharactersPreview() {
    RickAndMortyTheme {
        AllCharactersTopBar(
            onSearchIconClick = { }
        )
    }
}


@Preview(showBackground = true)
@Composable
fun TopBarCharactersSearchPreview() {
    RickAndMortyTheme {
        SearchTopBar(
            onInputBarChange = { },
            onInputTrailingIconClick = { },
            searchQuery = "Rick",
            onBackButtonClick = { },
        )
    }
}


@Preview(showBackground = true)
@Composable
fun TopBarFavoriteCharactersPreview() {
    RickAndMortyTheme {
        TopBarFavorites()
    }
}


@Preview(showBackground = true)
@Composable
fun TopBarDetailFavoritePreview() {
    RickAndMortyTheme {
        val rick = Character(
            1,
            "Rick Sanchez",
            "Alive",
            "Human",
            "-",
            "Male",
            "Earth (C-137)",
            "Earth (Replacement Dimension)",
            "",
            true
        )
        TopBarDetail(character = rick, {}, { _, _ -> })
    }
}


@Preview(showBackground = true)
@Composable
fun TopBarDetailBasicPreview() {
    RickAndMortyTheme {
        val rick = Character(
            1,
            "Rick Sanchez",
            "Alive",
            "Human",
            "-",
            "Male",
            "Earth (C-137)",
            "Earth (Replacement Dimension)",
            "",
            false
        )
        TopBarDetail(character = rick, {}, { _, _ -> })
    }
}