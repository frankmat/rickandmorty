package cz.cvut.fit.biand.rickandmorty

import androidx.room.Database
import cz.cvut.fit.biand.rickandmorty.data.db_room.model.DbCharacter
import cz.cvut.fit.biand.rickandmorty.data.db_room.CharactersDao
import cz.cvut.fit.biand.rickandmorty.data.db_room.model.CharacterFTS
import cz.cvut.fit.biand.rickandmorty.data.db_room.model.Favorite

@Database(entities = [DbCharacter::class, Favorite::class,CharacterFTS::class], version = 4)
abstract class RickAndMortyRoomDatabase : androidx.room.RoomDatabase() {
    abstract fun charactersDao(): CharactersDao
}

