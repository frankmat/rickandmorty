package cz.cvut.fit.biand.rickandmorty.data.db_room.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Fts4

@Entity(tableName = "characters_fts")
@Fts4(contentEntity = DbCharacter::class)
data class CharacterFTS(
    @ColumnInfo(name="name")
    val name:String
)
