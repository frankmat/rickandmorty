package cz.cvut.fit.biand.rickandmorty.viewmodel

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.*
import cz.cvut.fit.biand.rickandmorty.data.db_room.model.*
import cz.cvut.fit.biand.rickandmorty.repository.CharactersRepository
import cz.cvut.fit.biand.rickandmorty.helpers.CharactersScreenState
import cz.cvut.fit.biand.rickandmorty.helpers.Constants
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.*
import javax.inject.Inject

@HiltViewModel
class SearchViewModel @Inject constructor(private val charactersRepository: CharactersRepository) :
    ViewModel() {

    private val user = User(Constants.USERNAME)

    private val _topBarState = mutableStateOf(CharactersScreenState.ALL)
    val topBarState: State<CharactersScreenState> get() = _topBarState

    private val _searchQuery: MutableState<String> = mutableStateOf("")
    val searchQuery: State<String> get() = _searchQuery

    private val pagingConfig = PagingConfig(pageSize = Constants.PAGE_SIZE)

    @OptIn(ExperimentalPagingApi::class)
    val allCharactersFlow =
        Pager(pagingConfig, remoteMediator = charactersRepository.getAllCharactersMediator()) {
            charactersRepository.getAllCharactersPagingSource(user)
        }.flow.map { it.toPagingDataOfUICharacters() }.cachedIn(viewModelScope)

    val allCharactersFavoriteFlow =
        Pager(pagingConfig) {
            charactersRepository.getFavoriteCharactersPagingSource(user)
        }.flow.map { it.toPagingDataOfUICharacters() }.cachedIn(viewModelScope)

    @OptIn(ExperimentalCoroutinesApi::class)
    val searchCharactersFlow =
        Pager(pagingConfig) {
            charactersRepository.getSearchedCharactersByNamePagingSource(user, editQuery(searchQuery.value))
        }.flow.map{it.extractPagingDataOfUICharacters()}.cachedIn(viewModelScope)


    fun onSearchIconClick() {
        _topBarState.value = CharactersScreenState.SEARCH
    }

    fun onInputBarChange(newValue: String) {
        _searchQuery.value =newValue

    }

    fun onInputTrailingIconClick() {
        _searchQuery.value = ""
    }

    fun onBackButtonClick() {
        _topBarState.value = CharactersScreenState.ALL
    }

    private fun editQuery(newValue: String): String {
        if(newValue.isBlank()){
            return newValue
        }
        val queryWithEscapedQuotes = newValue.trim().replace(Regex.fromLiteral("\""), "\"\"")
        if(queryWithEscapedQuotes.first() == '-'){
            return "*\"$queryWithEscapedQuotes\"*"
        }
        else{
            return "*$queryWithEscapedQuotes*"
        }
    }
}



