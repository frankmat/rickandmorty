package cz.cvut.fit.biand.rickandmorty.helpers

enum class TelephoneMode(val detailPictureScale:Float) {
    Landscape(0.20f),Portrait(0.4f)
}