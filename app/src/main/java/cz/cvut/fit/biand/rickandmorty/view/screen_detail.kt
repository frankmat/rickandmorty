package cz.cvut.fit.biand.rickandmorty.view


import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import cz.cvut.fit.biand.rickandmorty.R
import cz.cvut.fit.biand.rickandmorty.viewmodel.DetailViewModel
import com.ramcosta.composedestinations.annotation.Destination
import com.ramcosta.composedestinations.navigation.DestinationsNavigator
import cz.cvut.fit.biand.rickandmorty.data.ui.Character
import cz.cvut.fit.biand.rickandmorty.helpers.TelephoneMode
import cz.cvut.fit.biand.rickandmorty.ui.theme.RickAndMortyTheme

@Destination
@Composable
fun DetailScreen(
    character: Character,
    destinationsNavigator: DestinationsNavigator,
    viewModel: DetailViewModel = hiltViewModel()
) {
    Scaffold(topBar = {
        TopBarDetail(
            character = character,
            onBackButtonClick = { destinationsNavigator.popBackStack()},
            onStarClick = viewModel::onStarClick
        )
    }) {
        DetailCard(character) { path, modifier ->
            OwnGlideImage(
                photo = path,
                modifier = modifier
            )
        }
    }
}


@Composable
fun DetailCard(character: Character, image: @Composable (String, Modifier) -> Unit) {
    Card(
        modifier = Modifier
            .padding(10.dp)
            .fillMaxSize(),
        shape = RoundedCornerShape(8.dp),
        elevation = 3.dp
    ) {
        DetailContent(character = character, image)
    }
}


@Composable
fun DetailContent(character: Character, image: @Composable (String, Modifier) -> Unit) {
    BoxWithConstraints {
        val telephoneMode =
            if (this.maxWidth < 550.dp) TelephoneMode.Portrait else TelephoneMode.Landscape
        val maxWidth = this.maxWidth

        Column {
            with(character) {
                Row(
                    Modifier
                        .fillMaxWidth()
                        .padding(16.dp)
                ) {

                    image(
                        photo, Modifier
                            .size(maxWidth * telephoneMode.detailPictureScale)
                            .clip(RoundedCornerShape(8.dp))
                    )

                    Column(
                        modifier = Modifier
                            .padding(start = 16.dp)
                            .fillMaxWidth(0.72f)
                    ) {
                        Text(
                            text = stringResource(id = R.string.name),
                            style = MaterialTheme.typography.subtitle2,
                            color = MaterialTheme.colors.secondaryVariant,
                            modifier = Modifier.padding(bottom = 14.dp)
                        )
                        Text(text = name, style = MaterialTheme.typography.h2)

                    }
                    if (telephoneMode == TelephoneMode.Landscape) {
                        Column {
                            DetailContentItem(
                                headline = stringResource(id = R.string.status),
                                value = status

                            )
                            DetailContentItem(
                                headline = stringResource(id = R.string.species),
                                value = species

                            )
                        }
                    }
                }
                Divider(
                    modifier = Modifier.fillMaxWidth(),
                    color = MaterialTheme.colors.onSecondary
                )
                if (telephoneMode == TelephoneMode.Portrait) {
                    DetailContentPortrait(status, species, type, gender, origin, location)
                } else {
                    DetailContentLandscape(type, gender, origin, location)
                }
            }
        }
    }
}


@Composable
fun DetailContentItem(modifier: Modifier = Modifier, headline: String, value: String,alignment:Alignment.Horizontal=Alignment.Start) {
    Column(modifier = modifier.padding(top = 17.dp, start = 8.dp, end = 8.dp), horizontalAlignment = alignment) {
        Text(
            text = headline,
            style = MaterialTheme.typography.body1,
            color = MaterialTheme.colors.secondaryVariant
        )
        Text(text = value, style = MaterialTheme.typography.h3)
    }
}

@Composable
fun DetailContentPortrait(
    status: String,
    species: String,
    type: String,
    gender: String,
    origin: String,
    location: String
) {
    val items = listOf(
        R.string.status to status,
        R.string.species to species,
        R.string.type to type,
        R.string.gender to gender,
        R.string.origin to origin,
        R.string.location to location
    )
    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(start = 8.dp)
    ) {
        for (item in items) {
            DetailContentItem(
                headline = stringResource(id = item.first),
                value = item.second
            )
        }
    }
}


@Composable
fun DetailContentLandscape(
    type: String,
    gender: String,
    origin: String,
    location: String
) {
    val items = listOf(
        R.string.type to type,
        R.string.gender to gender,
        R.string.origin to origin,
        R.string.location to location
    )
    Row(
        modifier = Modifier
            .fillMaxSize()
            .padding(start = 8.dp, end = 8.dp),
        horizontalArrangement = Arrangement.SpaceEvenly,
        verticalAlignment = Alignment.CenterVertically
    ) {
        for (item in items) {
            DetailContentItem(
                headline = stringResource(id = item.first),
                value = item.second,
                alignment = Alignment.CenterHorizontally

            )
        }
    }
}

@Preview(showBackground = true, widthDp = 571, heightDp = 350)
@Composable
fun DetailCardPreviewLandScape() {
    DetailCardPreviewContent()
}


@Preview(showBackground = true, widthDp = 350, heightDp = 571)
@Composable
fun DetailCardPreviewPortrait() {
    DetailCardPreviewContent()
}


@Composable
fun DetailCardPreviewContent() {
    val rick = remember {
        Character(
            1,
            "Abadango Cluster Princess",
            "Alive",
            "Human",
            "-",
            "Male",
            "Earth (C-137)",
            "Earth (Replacement Dimension)",
            "",
            true
        )
    }

    RickAndMortyTheme {
        DetailCard(character = rick) { photo, modifier ->
            Image(
                painter = painterResource(id = R.drawable.abadango),
                modifier = modifier,
                contentDescription = stringResource(id = R.string.photo_preview, photo)
            )
        }
    }
}

