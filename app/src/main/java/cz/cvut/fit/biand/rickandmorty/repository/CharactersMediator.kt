package cz.cvut.fit.biand.rickandmorty.repository

import androidx.paging.ExperimentalPagingApi
import androidx.paging.LoadType
import androidx.paging.PagingState
import androidx.paging.RemoteMediator
import androidx.room.withTransaction
import cz.cvut.fit.biand.rickandmorty.RickAndMortyRoomDatabase
import cz.cvut.fit.biand.rickandmorty.data.db_room.CharactersDao
import cz.cvut.fit.biand.rickandmorty.data.db_room.model.CharacterWithFavorite
import cz.cvut.fit.biand.rickandmorty.data.retrofit.RickAndMortyRemoteService
import cz.cvut.fit.biand.rickandmorty.data.retrofit.model.toDbCharacters
import cz.cvut.fit.biand.rickandmorty.helpers.Constants
import retrofit2.HttpException
import java.io.IOException

@OptIn(ExperimentalPagingApi::class)
class CharactersMediator(
    private val database: RickAndMortyRoomDatabase,
    private val charactersDao: CharactersDao,
    private val networkService: RickAndMortyRemoteService
) : RemoteMediator<Int, CharacterWithFavorite>() {

    override suspend fun initialize(): InitializeAction {
        return if (charactersDao.getNumberOfCharacters() == 0) {
            InitializeAction.LAUNCH_INITIAL_REFRESH
        } else {
            InitializeAction.SKIP_INITIAL_REFRESH
        }
    }

    override suspend fun load(
        loadType: LoadType,
        state: PagingState<Int, CharacterWithFavorite>
    ): MediatorResult {
        return try {
            val loadKey = when (loadType) {
                LoadType.REFRESH -> null
                LoadType.PREPEND -> return MediatorResult.Success(endOfPaginationReached = true)
                LoadType.APPEND -> {
                    val lastItem = state.lastItemOrNull()
                    if (lastItem == null && state.anchorPosition == null) {
                        return MediatorResult.Success(endOfPaginationReached = false)
                    }
                    if (lastItem == null) {
                        return MediatorResult.Success(endOfPaginationReached = true)
                    }
                    lastItem.id
                }
            }

            val page: Int = if (loadKey == null) 1 else (loadKey / Constants.PAGE_SIZE) + 1
            val response = networkService.getCharacters(page)
            database.withTransaction {
                if (loadType == LoadType.REFRESH) {
                    charactersDao.deleteCharacters()
                }
                charactersDao.insertCharacters(response.results.toDbCharacters())
            }
            MediatorResult.Success(endOfPaginationReached = response.info.next == null)
        } catch (e: IOException) {
            MediatorResult.Error(e)
        } catch (e: HttpException) {
            MediatorResult.Error(e)
        }
    }
}

