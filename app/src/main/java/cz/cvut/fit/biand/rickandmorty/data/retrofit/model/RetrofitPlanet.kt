package cz.cvut.fit.biand.rickandmorty.data.retrofit.model

import com.google.gson.annotations.SerializedName

data class RetrofitPlanet(
    @SerializedName("name")
    val name:String,
    @SerializedName("url")
    val url:String
    )